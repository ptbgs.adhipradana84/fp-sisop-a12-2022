#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <arpa/inet.h>
#define PORT 8080

int sock = 0;

struct UserSchema
{
    char name[10];
    char database[20];
};
struct UserSchema user;

void sendMessage(char message[])
{
    int status;
    if (fork() == 0)
        send(sock, message, strlen(message), 0);

    while ((wait(&status)) > 0)
        ;
}

bool startsWith(const char *a, const char *b)
{
    if (strncmp(a, b, strlen(b)) == 0)
        return 1;
    return 0;
}

bool isUserRoot()
{
    return getuid() ? false : true;
}

bool isSyntaxValid(char real[], char command[])
{
    if (strcmp(real, command) != 0)
    {
        printf("Unreconized command: %s\n\n", command);
        return false;
    }
    return true;
}

bool isAuthenticate(int argc, char *argv[])
{
    if (argc < 5)
        return false;

    if (strcmp(argv[1], "-u") != 0)
        return false;
    sendMessage(argv[2]);
    sleep(1);

    if (strcmp(argv[3], "-p") != 0)
        return false;

    sendMessage(argv[4]);

    char response[20] = {0};
    read(sock, response, 20);

    if (strcmp(response, "USERAUTH-FAIL") == 0)
        return false;

    strcpy(user.name, response);

    return true;
}

void rootCreateNewUser(char command[])
{
    char *token = strtok(command, " ");
    if (!isSyntaxValid("CREATE", token))
        return;

    token = strtok(NULL, " ");
    if (!isSyntaxValid("USER", token))
        return;

    char username[100] = {0};
    token = strtok(NULL, " ");
    strcpy(username, token);

    token = strtok(NULL, " ");
    if (!isSyntaxValid("IDENTIFIED", token))
        return;

    token = strtok(NULL, " ");
    if (!isSyntaxValid("BY", token))
        return;

    char password[100] = {0};
    token = strtok(NULL, " ");
    strcpy(password, token);

    sendMessage("CREATE");
    sendMessage(username);
    sendMessage(password);
}

void rootGrantPermission(char command[])
{
    char *token = strtok(command, " ");
    if (!isSyntaxValid("GRANT", token))
        return;

    token = strtok(NULL, " ");
    if (!isSyntaxValid("PERMISSION", token))
        return;

    char databaseName[100] = {0};
    token = strtok(NULL, " ");
    strcpy(databaseName, token);

    token = strtok(NULL, " ");
    if (!isSyntaxValid("INTO", token))
        return;

    char username[100] = {0};
    token = strtok(NULL, " ");
    strcpy(username, token);

    sendMessage("GRANT");
    sendMessage(username);
    sendMessage(databaseName);
}

void categorizeRootCommand(char command[])
{
    sendMessage(command);

    if (startsWith(command, "CREATE USER"))
        rootCreateNewUser(command);
    else if (startsWith(command, "GRANT PERMISSION"))
        rootGrantPermission(command);
    else
        printf("ERROR Unrecognized command: %s\n\n", command);
}

void userUseDatabase(char command[])
{
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");

    sendMessage("USE-DB");
    sleep(1);
    sendMessage(token);
    sleep(1);

    char response[10] = {0};
    read(sock, response, 10);

    if (strcmp(response, "USE-1") == 0)
    {
        strcpy(user.database, token);
        printf("You (%s) use %s\n", user.name, token);
    }
    else
    {
        printf("You (%s) does not have access to %s\n", user.name, token);
    }
}

void userCreateDatabase(char command[])
{
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    sendMessage("CREATE-DB");
    sleep(1);
    sendMessage(token);
}

void userCreateTable(char command[])
{
    sendMessage("CREATE-TB");
    sleep(1);
    sendMessage(command);

    char response[10] = {0};
    read(sock, response, 10);

    if (strcmp(response, "CREATE-TB1") == 0)
        printf("Table created successfully\n");
    else
        printf("Table failed to create\n");
}

void userSelectAllColumn(char command[])
{
    sendMessage("SELECT-A");
    sleep(1);
    sendMessage(command);

    char response[50] = {0};

    puts("");
    while (strcmp(response, "SELECT-9") != 0)
    {
        printf("%s", response);
        memset(response, 0, sizeof(response));
        read(sock, response, 50);
    }
    puts("");
}

void userSelectSomeColumn(char command[])
{
    sendMessage("INSERT-S");
    sleep(1);
    sendMessage(command);
}

void userDropColumn(char command[])
{
}

void userDropTable(char command[])
{
}

void userDropDatabase(char command[])
{
    sendMessage("DROP-DB");
    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");
    sendMessage(token); // database name

    char response[10] = {0};
    read(sock, response, 10);

    if (strcmp(response, "DROP-DB1") == 0)
        printf("Table %s successfuly dropped\n", token);
    if (strcmp(response, "DROP-DB2") == 0)
        printf("You have no access to %s database\n", token);
    else
        printf("Error occured\n");
}

void userInsertData(char command[])
{
    sendMessage("INSERT");
    sleep(1);
    sendMessage(command);

    char response[10] = {0};
    read(sock, response, 10);

    if (strcmp(response, "INSERT-1") == 0)
        printf("Data successfully inserted\n");
    else
        printf("Data failed to insert\n");
}

void userUpdateData(char command[])
{
}

void userDeleteTable(char command[])
{
}

void categorizeUserCommand(char command[])
{
    sendMessage(command);
    if (startsWith(command, "USE"))
        userUseDatabase(command);
    else if (startsWith(command, "CREATE DATABASE"))
        userCreateDatabase(command);
    else if (startsWith(command, "CREATE TABLE"))
        userCreateTable(command);
    else if (startsWith(command, "SELECT * FROM"))
        userSelectAllColumn(command);
    else if (startsWith(command, "SELECT "))
        userSelectSomeColumn(command);
    else if (startsWith(command, "DROP COLUMN"))
        userDropColumn(command);
    else if (startsWith(command, "DROP TABLE"))
        userDropTable(command);
    else if (startsWith(command, "DROP DATABASE"))
        userDropDatabase(command);
    else if (startsWith(command, "INSERT INTO"))
        userInsertData(command);
    else if (startsWith(command, "UPDATE"))
        userUpdateData(command);
    else if (startsWith(command, "DELETE FROM"))
        userDeleteTable(command);
    else
        printf("ERROR %s Unrecognized command: %s\n\n", user.name, command);

    printf("LOG: %s\n", command);
    sendMessage(command);
}

int main(int argc, char *argv[])
{
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    // MY CUSTOM CODE STARTS HERE

    printf("Welcome to SeaSoup DBMS!!!\n\n");

    if (isUserRoot()) // if ROOT
    {
        sendMessage("ROOT");
        strcpy(user.name, "ROOT");
    }
    else if (!isAuthenticate(argc, argv)) // if USER
    {
        printf("Username/Password invalid!\n\n");
        exit(1);
    }

    while (true)
    {
        char command[255] = {0};
        if (isUserRoot())
        {
            printf("ROOT > ");
            gets(command);

            if (strcmp(command, "exit") == 0)
                break;

            command[strlen(command) - 1] = '\0';
            categorizeRootCommand(command);
        }
        else
        {
            printf("USER > ");
            gets(command);

            if (strcmp(command, "exit") == 0)
                break;

            command[strlen(command) - 1] = '\0';
            categorizeUserCommand(command);
        }
    }

    printf("\nClient Disconnected...\n\n");

    return 0;
}