#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define PORT 8080

int new_socket;
bool isLoggedIn = false;
bool isUsingDatabase = false;

struct UserSchema
{
    char name[10];
    char database[20];
    bool isRoot;
};
struct UserSchema user;

bool startsWith(const char *a, const char *b)
{
    if (strncmp(a, b, strlen(b)) == 0)
        return 1;
    return 0;
}

void sendMessage(char message[])
{
    int status;
    if (fork() == 0)
        send(new_socket, message, strlen(message), 0);

    while ((wait(&status)) > 0)
        ;
}

void makeDirectory(char path[])
{
    int status;
    if (fork() == 0)
        execlp("mkdir", "mkdir", "-p", path, NULL);
    while ((wait(&status)) > 0)
        ;
}

void deleteDirectory(char path[])
{
    int status;
    if (fork() == 0)
        execlp("rm", "rm", "-r", path, NULL);
    while ((wait(&status)) > 0)
        ;
}

void makeFile(char fileName[])
{
    int status;
    if (fork() == 0)
    {
        execlp("touch", "touch", fileName, NULL);
    }
    while ((wait(&status)) > 0)
        ;
}

void writeToFile(char path[], char content[])
{
    FILE *fp;
    fp = fopen(path, "a");

    fputs(content, fp);

    fclose(fp);
}

void rootCreateUser()
{
    char username[20] = {0}, password[20] = {0};
    read(new_socket, username, 20);
    read(new_socket, password, 20);

    char content[41] = {0};
    sprintf(content, "%s|%s", username, password);

    FILE *fp;
    fp = fopen("./databases/users/users", "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    fputs(content, fp);
    fputs("\n", fp);

    fclose(fp);
}

void rootGrantPermission()
{
    char username[20] = {0}, database[20] = {0};
    read(new_socket, username, 20);
    read(new_socket, database, 20);

    char content[41] = {0};
    sprintf(content, "%s|%s", username, database);

    FILE *fp;
    fp = fopen("./databases/users/permissions", "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    fputs(content, fp);
    fputs("\n", fp);

    fclose(fp);
}

void userCreateDatabase()
{
    char databaseName[20] = {0};
    read(new_socket, databaseName, 20);

    // create database
    char dbPath[100] = {0};
    sprintf(dbPath, "%s%s", "./databases/", databaseName);
    makeDirectory(dbPath);
    // add permission
    char permission[100] = {0};
    sprintf(permission, "%s|%s", user.name, databaseName);

    FILE *fp;
    fp = fopen("./databases/users/permissions", "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    fputs(permission, fp);
    fputs("\n", fp);

    fclose(fp);
}

bool isUserHasAccessToDatabase(char dbName[])
{
    char permission[31] = {0};
    sprintf(permission, "%s|%s", user.name, dbName);

    FILE *fp;
    fp = fopen("./databases/users/permissions", "r");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    char str[31] = {0};

    fgets(str, 31, fp);
    while (fgets(str, 31, fp) != NULL)
    {
        str[strlen(str) - 1] = '\0';
        if (strcmp(permission, str) == 0)
        {
            fclose(fp);
            return true;
        }
    }

    fclose(fp);
    return false;
}

void userUseDatabase()
{
    char dbName[20] = {0};
    read(new_socket, dbName, 20);

    if (isUserHasAccessToDatabase(dbName))
    {
        strcpy(user.database, dbName);
        isUsingDatabase = true;
        sendMessage("USE-1");
    }
    else
    {
        sendMessage("USER-0");
    }
}

void addTableHeader(char tableName[], char content[], bool useBorder, bool useEnter)
{
    char tablePath[100] = {0};
    sprintf(tablePath, "%s%s%s%s", "./databases/", user.database, "/", tableName);

    FILE *fp;
    fp = fopen(tablePath, "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    if (useBorder)
        fputs("|", fp);
    fputs(content, fp);
    if (useEnter)
        fputs("\n", fp);

    fclose(fp);
}

void addTableType(char tableName[], char content[])
{
    char tableTypePath[100] = {0};
    sprintf(tableTypePath, "%s%s%s%s", "./databases/", user.database, "/.", tableName);

    FILE *fp;
    fp = fopen(tableTypePath, "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    fputs(content, fp);
    fputs("\n", fp);

    fclose(fp);
}

void userCreateTable()
{
    char tableName[20] = {0};
    char command[120] = {0};
    read(new_socket, command, 120);

    char *token = strtok(command, " ");
    token = strtok(NULL, " ");

    token = strtok(NULL, " ");
    strcpy(tableName, token);

    char tablePath[100] = {0};
    sprintf(tablePath, "%s%s%s%s", "./databases/", user.database, "/", token);
    makeFile(tablePath);

    token = strtok(NULL, ")");

    token++; // remove (

    token = strtok(token, ",");

    char *temp = token;

    char type[20] = {0};

    char *token2 = strtok_r(temp, " ", &temp);

    strcpy(type, token2); // column

    addTableHeader(tableName, token2, false, false);

    token2 = strtok_r(NULL, " ", &temp);
    strcat(type, "|");    // |
    strcat(type, token2); // type

    addTableType(tableName, type);

    while (true)
    {
        memset(type, 0, sizeof(type));

        token = strtok(NULL, ",");
        if (token == NULL)
        {
            printf("exit\n");
            break;
        }
        token++;

        temp = token;
        char *token2 = strtok_r(temp, " ", &temp);

        strcpy(type, token2); // column
        addTableHeader(tableName, token2, true, false);

        token2 = strtok_r(NULL, " ", &temp);
        strcat(type, "|");    // |
        strcat(type, token2); // type

        addTableType(tableName, type);
    }

    addTableHeader(tableName, "", false, true); // add enter
    sendMessage("CREATE-TB1");
}

void insertDataToTable(char tableName[], char content[])
{
    char tablePath[100] = {0};
    sprintf(tablePath, "%s%s%s%s", "./databases/", user.database, "/", tableName);

    FILE *fp;
    fp = fopen(tablePath, "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    fputs(content, fp);
    fputs("\n", fp);

    fclose(fp);
}

void userSelectAllColumn()
{
    char command[120] = {0};
    read(new_socket, command, 120);

    char *token = strtok(command, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");

    token = strtok(NULL, " "); // table name

    char tablePath[100] = {0};
    sprintf(tablePath, "%s%s%s%s", "./databases/", user.database, "/", token);

    FILE *fp;
    fp = fopen(tablePath, "r");

    char str[50] = {0};

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    while (fgets(str, 50, fp) != NULL)
    {
        sendMessage(str);
    }
    sendMessage("SELECT-9");

    fclose(fp);
}

void userSelectSomeColumn()
{
}

void userInsertData()
{
    char content[100] = {0};
    char tableName[20] = {0};
    char command[120] = {0};
    read(new_socket, command, 120);

    char *token = strtok(command, " ");
    token = strtok(NULL, " ");

    token = strtok(NULL, " "); // table name
    strcpy(tableName, token);

    token = strtok(NULL, ")");
    token++;

    token = strtok(token, ",");

    if (token[0] == '\'')
    {
        token[strlen(token) - 1] = '\0';
        token++;
    }

    strcpy(content, token);

    while (token != NULL)
    {
        token = strtok(NULL, ",");

        if (token == NULL)
            break;

        token++;

        if (token[0] == '\'')
        {
            token[strlen(token) - 1] = '\0';
            token++;
        }

        strcat(content, "|");
        strcat(content, token);
    }

    insertDataToTable(tableName, content);
    sendMessage("INSERT-1");
}

void userDropDatabase()
{
    char databaseName[20] = {0};
    read(new_socket, databaseName, 20);

    if (isUserHasAccessToDatabase(databaseName))
    {
        char dbPath[100] = {0};
        sprintf(dbPath, "%s%s", "./databases/", databaseName);
        deleteDirectory(dbPath);
        sendMessage("DROP-DB1");
    }
    else
    {
        sendMessage("DROP-DB2");
    }
}

void writeToDBLog()
{
    char command[120] = {0};
    read(new_socket, command, 120);

    printf("LOG: %s\n", command);

    FILE *fp;
    fp = fopen("./db.log", "a");

    if (fp == NULL)
    {
        perror("Users file can't be opened");
    }

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    char logContent[150] = {0};
    sprintf(logContent, "%d-%02d-%02d %02d:%02d:%02d:%s:%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, user.name, command);

    fputs(logContent, fp);
    fputs("\n", fp);

    fclose(fp);
}

void categorizeUserCommand(char command[])
{
    if (startsWith(command, "CREATE-DB"))
        userCreateDatabase();
    else if (startsWith(command, "USE-DB"))
        userUseDatabase();
    else if (startsWith(command, "CREATE-TB"))
        userCreateTable();
    else if (startsWith(command, "SELECT-A"))
        userSelectAllColumn();
    else if (startsWith(command, "SELECT-S"))
        userSelectSomeColumn();
    else if (startsWith(command, "INSERT"))
        userInsertData();
    else if (startsWith(command, "DROP-DB"))
        userDropDatabase();
}

void categorizeRootCommand(char command[])
{
    if (strcmp(command, "CREATE") == 0)
        rootCreateUser();
    else if (strcmp(command, "GRANT") == 0)
        rootGrantPermission();

    writeToDBLog();
}

bool isUserAuthenticated(char username[], char password[])
{
    char userData[50] = {0};
    sprintf(userData, "%s|%s", username, password);

    FILE *ptr;
    char str[41];
    ptr = fopen("./databases/users/users", "a+");

    if (NULL == ptr)
    {
        perror("Users file can't be opened\n");
    }

    fgets(str, 41, ptr);
    while (fgets(str, 41, ptr) != NULL)
    {
        str[strlen(str) - 1] = '\0';
        if (strcmp(userData, str) == 0)
        {
            fclose(ptr);
            return true;
        }
    }

    fclose(ptr);
    return false;
}

void authenticate()
{
    char username[10] = {0};
    read(new_socket, username, 10);

    if (strcmp(username, "ROOT") == 0)
    {
        strcpy(user.name, "ROOT");
        user.isRoot = true;
        isLoggedIn = true;
    }
    else
    {
        char password[10] = {0};
        read(new_socket, password, 10);

        if (isUserAuthenticated(username, password))
        {
            strcpy(user.name, username);
            user.isRoot = false;
            isLoggedIn = true;
            sendMessage(username);
        }
        else
            sendMessage("USERAUTH-FAIL");
    }
}

void makeTable(char path[], char column[])
{
    FILE *fp;
    fp = fopen(path, "a");
    fputs(column, fp);
    fputs("\n", fp);
    fclose(fp);
}

void setUpBaseDatabase()
{
    makeDirectory("./databases");

    makeDirectory("./databases/users");

    makeFile("./db.log");

    if (access("./databases/users/users", F_OK) != 0)
        makeTable("./databases/users/users", "NAME|PASSWORD");
    if (access("./databases/users/permissions", F_OK) != 0)
        makeTable("./databases/users/permissions", "NAME|DATABASE");
}

int main(int argc, char const *argv[])
{
    pid_t pid, sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
     * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
     * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    // if ((chdir("/")) < 0)
    // {
    //     exit(EXIT_FAILURE);
    // }

    // SERVER

    int server_fd;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // MY CUSTOM CODE STARTS HERE
    setUpBaseDatabase();
    authenticate();

    while (1)
    {
        // MY CUSTOM CODE FOR SERVER STARTS HERE
        while (isLoggedIn) // if LOGGED IN
        {
            writeToDBLog();
            char command[10] = {0};
            read(new_socket, command, 10);

            if (strcmp(user.name, "ROOT") == 0)
                categorizeRootCommand(command);
            else
                categorizeUserCommand(command);
        }
    }

    printf("\nServer Disconnected...\n\n");
    return 0;
}